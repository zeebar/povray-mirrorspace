
# povray-mirrorspace

The aim of this project is to visualize a very simple mirror space consisting of a circular arrangement of square vertical mirrors.

The inside of the space consists of a baby GNU and Tux (an adorable penguin).

Throughout the animation, more mirrors are added behind the pair, and the space changes accordingly.


# Status

The animation needs a lot of tweaking and should probably be driven by something smarter than a Makefile.


# TODO

* dual mode rendering:  proof and production
* change localtion of camera
* change size of characters
* add titles
* add music and tweak timing


# Credits

idea, mirror space pov code by Helmut Hissen helmut@zeebar.com, for the SMUS Jr school in Victoria, BC 2017

inspired by mirror space artist Yayoi Kusama https://www.nytimes.com/2017/02/23/arts/design/yayoi-kusama-infinity-mirrors.html

gnu-tux courtesy of Nicolas P. Rougier https://www.labri.fr/perso/nrougier/artwork/

lego tardis from http://ldd.us.lego.com/en-us/gallery?memberid=therob123 and other tools from http://www.ldraw.org/reference/tutorials/rendering/101/, if only I could get those to work as well :-)

