
FIRST_FRAME =   0
LAST_FRAME  =  30

START_CLOCK = 0.0
END_CLOCK   = 1.0

FRAMES_DIR  = ./frames

#RENDER_OPTS = +A

WIDTH       = 600
HEIGHT      = 480

VENDOR_DIR = ./vendor

RENDERED_FRAMES = $(FRAMES_DIR)/.rendered

GNU_TUX_TGZ = $(VENDOR_DIR)/gnu-tux.tgz
GNU_TUX_URL = https://www.labri.fr/perso/nrougier/downloads/gnu-tux.tgz
GNU_TUX_DIR = ./gnu-tux

GT_ANIM_AVI  = gnutux.avi
GT_ANIM_GIF  = gnutux.gif
GT_ANIM_MPEG = gnutux.mpeg
GT_ANIM_MP4  = gnutux.mp4

GT_MOVIES   = $(GT_ANIM_AVI) $(GT_ANIM_GIF) $(GT_ANIM_MP4) $(GT_ANIM_MPEG)

$(GT_ANIM_GIF): $(GT_ANIM_AVI)
	- /bin/rm $@
	ffmpeg -i $(GT_ANIM_AVI) -pix_fmt rgb24 -loop 0 $@

$(GT_ANIM_MP4): $(GT_ANIM_AVI)
	- /bin/rm $@
	ffmpeg -i $(GT_ANIM_AVI) -loop 0 $@

$(GT_ANIM_AVI): $(RENDERED_FRAMES)
	- /bin/rm $@
	ffmpeg -f image2 -i $(FRAMES_DIR)/gnutux%03d.png $@

$(RENDERED_FRAMES):(FRAMES_DIR)/.rendered: $(GT_POV)
    - mkdir $(FRAMES_DIR)
	povray +I$(GT_POV) +L$(GNU_TUX_DIR)./gnu-tux +O$(FRAMES_DIR)/gnutux.png +W${WIDTH} +H${HEIGHT} +KFI$(FIRST_FRAME)  +KFF$(LAST_FRAME) +KI$(START_CLOCK) +KF$(END_CLOCK)  $(RENDER_OPTS)
	touch $@

$(GNU_TUX_TGZ):
	- mkdir $(VENDOR_DIR)
	wget $(GNU_TUX_URL) -o $(GNU_TUX_TGZ)

$(GNU_TUX_DIR): $(GNU_TUX_TGZ)
	tar xvzf $(GNU_TUX_TGZ)

clean: 
	/bin/rm -f $(GNU_TUX_DIR) $(FRAMES_DIR) $(GT_MOVIES)

distclean: clean
	/bin/rm -f $(GNU_TUX_TGZ)

