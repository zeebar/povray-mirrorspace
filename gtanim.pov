
#version 3.7;

#include "colors.inc"
#include "glass.inc"
#include "golds.inc"
#include "metals.inc"
#include "stones.inc"
#include "woods.inc"
#include "math.inc"


#declare use_radiosity = on;

// ========================================
//  Settings
// ========================================
global_settings {
//  assumed_gamma 1.0
  #if (use_radiosity)
    radiosity {
      pretrace_start 0.08
      pretrace_end 0.005
      count 600
      error_bound 0.05
      nearest_count 5
      recursion_limit 1
      low_error_factor .5
      gray_threshold 0.0
      minimum_reuse 0.015
      brightness 1.0
      adc_bailout 0.01/2
//      normal on
    }
  #end

  ambient_light White 
  max_trace_level 16
}

// ========================================
//  Default settings
// ========================================
#default {
  texture {
    #if (use_radiosity)
      finish {
        ambient 0.0
        diffuse 1.0
      }
    #else
      finish {
        ambient    0.1
        diffuse    0.6
      }
    #end
  }
}

#declare maxMirrors    =  12;
#declare maxMirrorSize =  4.0;
#declare moviePadding  =  1 / maxMirrors;

#declare maxMirrorHeight =  maxMirrorSize;
#declare maxMirrorWidth  =  maxMirrorSize;

#declare frameDepth      =  0.01;
#declare frameInset      =  0.02;
#declare frameRise       =  0.001;
#declare mirrorThickness =  0.001;

#declare beginSlide      =  0.10;
#declare endSlide        =  0.30;
#declare beginRise       =  0.35;
#declare endRise         =  0.50;
#declare beginWiden      =  0.55;
#declare endWiden        =  0.90;

#declare postHeight      =  0.12;

#macro Mirror ( mirrorWidth, mirrorHeight )

  #if ( (mirrorHeight > 2 * frameInset) & (mirrorWidth > 2 * frameInset) )
    union {
      difference {
        box { <0.0, 0.0, -frameRise>, <mirrorWidth, mirrorHeight, frameDepth> }
        box { 
         <frameInset, frameInset, -(frameRise + 0.001)>, 
         <mirrorWidth - frameInset, mirrorHeight - frameInset, frameDepth - mirrorThickness> 
        }
        texture { T_Frame }
      }
      box { 
        <frameInset,frameInset,0>,
        <mirrorWidth-frameInset,mirrorHeight-frameInset,mirrorThickness> 
        texture { T_Mirror }
      }
      translate <-mirrorWidth / 2, 0, 0>
    }    
  #else
    #if ( mirrorHeight > 0 ) 
      box { 
        <0.0,0.0,-frameRise>, <frameInset,mirrorHeight,frameDepth> 
        texture { T_Frame }
        translate <-mirrorWidth / 2, 0, 0>
      }
    #end
  #end

#end
  
#declare mtpm = moviePadding / 2;

#declare mtp0 = 0.0;
#declare mtp1 = 0.0;

#declare notPadding = 1.0 - 2 * moviePadding;
#declare clockPerMirror = notPadding / maxMirrors;

#if ( clock < moviePadding ) 
  #declare mt = 0.0;
  #if ( clock < mtpm ) 
    #declare mtp0 = 1.0;
  #else
    #declare mtp0 = (moviePadding - clock) / mtpm;
  #end
#else
  #if ( clock > 1.0 - moviePadding ) 
    #if ( clock > 1.0 - mtpm )  
      #declare mtp1 = 1.0;
    #else
      #declare mtp1 = ((1.0 - mtpm) - clock) / mtpm;
    #end
    #declare mt = maxMirrors;
  #else
    #declare mt = (clock - moviePadding) / notPadding * maxMirrors;
  #end
#end


#declare mn = floor( mt );
#declare md = mt - mn;


#if ( md <= endRise )
  #if ( md < beginRise )
    #declare mh = 0.0;
  #else
    #declare mh = (md - beginRise) / (endRise - beginRise);
  #end
#else 
  #declare mh = 1.0;
#end

#if ( md <= endWiden )
  #if ( md <= beginWiden )
    #declare mw = 0.0;
  #else
    #declare mw = (md - beginWiden) / (endWiden - beginWiden);
  #end
#else 
  #declare mw = 1.0;
#end

#if ( md <= endSlide )
  #if ( md <= beginSlide )
    #declare ms = 0.0;
  #else
    #declare ms = (md - beginSlide) / (endSlide - beginSlide);
  #end
#else 
  #declare ms = 1.0;
#end


#declare fw = maxMirrorWidth / 2;
#declare fd = frameDepth;

#declare mn0 = mn;
#declare mn1 = mn + 1;

#if ( mn0 < 3 )
  #declare mn0 = 3;
#end

#if ( mn1 < 3 )
  #declare mn1 = 3;
#end


#declare mrBase = 0.18;
#declare mrSlope = 0.15;

#declare mr0 = maxMirrorWidth * cos( pi / mn0) / ( 2 * sin( pi / mn0 ) );
#declare mr1 = maxMirrorWidth * cos( pi / mn1) / ( 2 * sin( pi / mn1 ) ); 

#declare mrm0 = maxMirrorWidth * (mrBase + mrSlope * cos( pi / 3 ) / ( 2 * sin( pi / 3 ) )); 
#declare wrm0 = 1.0 - (1.0 - mtp0) * (1.0 - mtp0);

#declare mrm1 = maxMirrorWidth * (mrBase + mrSlope * cos( pi / ( maxMirrors + 3 ) ) / ( 2 * sin( pi / (maxMirrors + 3 ) ) )); 
#declare wrm1 = 1.0 - (1.0 - mtp1) * (1.0 - mtp1);

#declare mrmn = maxMirrorWidth * (mrBase + mrSlope * cos( pi / ( mt + 3 ) ) / ( 2 * sin( pi / ( mt + 3 ) ) ));
#declare wrmn = 1.0 - (wrm0 + wrm1);

#declare mrc = wrm0 * mrm0 + wrm1 * mrm1;

#if ( wrmn > 0.0 ) 
  #declare mrc = mrc +  wrmn * mrmn;
#end


#declare am0 = pi / mn0;

#if ( mn >= 1 )
  #declare an0 = 2 * pi / mn;
#else
  #declare an0 = 0;
#end

#declare am1 = pi / mn1;
#declare an1 = 2 * pi / (mn + 1);

#declare mr = (mr0 * (1.0 - ms)) + (mr1 * ms);
#declare am = (am0 * (1.0 - ms)) + (am1 * ms);
#declare an = (an0 * (1.0 - ms)) + (an1 * ms);

#declare am_degs = am * 180 / pi;
#declare an_degs = an * 180 / pi;


// ========================================
//  Lights
// ========================================


light_source {
  <-1.0, 5, -0.6> 
  color rgb <1.0, 0.99, 0.98>*0.6
  area_light <1,0,0> <0,0,1> 1, 1 
  jitter 
  adaptive 2
}

// ========================================
//  Camera
// ========================================
#declare EyePos  = <0,  1.0 - 0.5 / mrc, -mrc>;
#declare EyeLook = <0,  0.9 - 0.4 / mrc,    0>;


camera {
  location EyePos
  look_at EyeLook
}


// ========================================
//  Sky
// ========================================
sky_sphere {pigment {rgb 1}}


// ========================================
//  Ground
// ========================================
plane {y,0  texture {pigment {rgb 1} finish {ambient 0.1 diffuse 1}}}
plane {z,100 inverse texture {pigment {rgb 1} finish {ambient 0.1 diffuse 1}}}

// ========================================
//  Spheric to cartesian conversion macro
// ========================================
#macro spheric (rho,theta,phi)
<rho*sin(theta)*sin(phi), rho*cos(theta), rho*sin(theta)*cos(phi)>
#end

// ========================================
// Gnu
// ========================================
#include "furtex.inc"

#declare t_fur       = FurTex (color <60,40,20>*3/256)
#declare t_fur_white = FurTex (color <60,40,20>*4/256)
#declare t_eye_white = texture {pigment{rgb 1}}
#declare t_eye_black = texture {pigment{rgb 0.01} finish {specular 1 roughness 0.005}}
#declare c_horn      = rgb<15,10,5>*1.0/255; //rgb<1,0.8,0.6>*0.2;
#declare c_hand      = rgb<15,10,5>*4.0/255;
#declare c_foot      = rgb<15,10,5>*4.0/255;
#declare t_horn = texture {
  pigment {
	wood
	turbulence 0.5
	color_map {
	  [0.25 color c_horn]
	  [0.50 color c_horn*0.95]
	  [0.75 color c_horn*1.05]
	}
  }
  scale <10,.5,.5>
  finish {specular .1}
}
#declare t_hand = texture {
  pigment {
	wood
	turbulence 0.5
	color_map {
	  [0.25 color c_hand]
	  [0.50 color c_hand*0.95]
	  [0.75 color c_hand*1.05]
	}
  }
  scale <10,.5,.5>
  finish {specular .1}
}
#declare t_foot = texture {
  pigment {
	wood
	turbulence 0.5
	color_map {
	  [0.25 color c_foot]
	  [0.50 color c_foot*0.95]
	  [0.75 color c_foot*1.05]
	}
  }
  scale <10,.5,.5>
  finish {specular .1}
}

#include "meshgnu.inc"
object {
  gnu
  scale 0.3
  rotate -x*90
  rotate y*180
  rotate -y*5
  translate -x * 0.4
  translate y * postHeight
}


// ========================================
// Tux
// ========================================
#declare t_fur_black = FurTex (color <1,1,1>*0.10)
#declare t_fur_white = FurTex (color <1,1,1>*1.01)
#declare t_eye_white = texture {pigment{rgb 1}}
#declare t_eye_black = texture {pigment{rgb 0.01} finish {specular 1 roughness 0.005}}
#declare t_beak      = texture {pigment {rgb <1,.55,0>}}
#declare t_tongue    = texture {pigment {rgb <1,0,0>} finish {specular .5 roughness 0.005}}
#declare t_feet      = texture {pigment {rgb <1,.55,0>}}
#include "tux.inc"

object {
  tux
  scale 0.3
  rotate y * 5
  translate x * 0.4
  translate y * postHeight
}


#declare T_Frame = texture {
  pigment {
    color rgb <0.95,1.0,1.0>
  }
  finish{
    diffuse      0.2
    ambient      0.8
    specular     0.1
    reflection {
      0.1
    }
    phong        0.1
    phong_size  10
  }
}

#declare T_Mirror = texture {
  pigment {
    color rgb <0.95,0.95,1.0>
  }
  finish{
    diffuse      0.01
    ambient      0.0
    specular     0.0
    reflection {
      0.98
    }
    phong        0.0
    phong_size 100
    // conserve_energy
  }
}


#declare hpw = fw / 2.4;

box { 
  <0 - hpw, postHeight - fd, 0 - hpw>, <hpw, postHeight, hpw>
  pigment { color rgb <0.8,0.5,0.1> } finish{ diffuse 0.6 ambient 0.5 }
  //texture { T_Frame }
}

#declare postIndent = 0.02;

box { 
  <0 - (hpw - postIndent), 0, 0 - (hpw - postIndent)>, <0 - (hpw - 2 * postIndent), postHeight - fd, hpw - postIndent>
  pigment { color rgb <0.95,0.2,0.1> } finish{ diffuse 0.6 ambient 0.5 }
}

box { 
  <hpw - 2 * postIndent, 0, 0 - (hpw - postIndent)>, <hpw - postIndent, postHeight - fd, hpw - postIndent>
  pigment { color rgb <0.3,0.95,0.1> } finish{ diffuse 0.6 ambient 0.5 }
}

box { 
  <0 - (hpw - postIndent), 0, hpw - 2 * postIndent>, <hpw - postIndent, postHeight - fd, hpw * postIndent>
  pigment { color rgb <0.95,0.95,0.3> } finish{ diffuse 0.6 ambient 0.5 }
}

box { 
  <0 - (hpw - postIndent), 0, 0 - (hpw - postIndent)>, <hpw - postIndent, postHeight - fd, 0 - (hpw - 2 * postIndent)>
  pigment { color rgb <0.1,0.2,0.98> } finish{ diffuse 0.6 ambient 0.5 }
}


#debug concat( 
   "clock", str( clock, 5, 2 ), 
   " mt ",  str( mt, 5, 2 ), 
   " mn ",  str( mn, 5, 2 ), 
   " md ",  str( md, 5, 2 ), 
   " ms ",  str( ms, 5, 2 ), 
   " mw ",  str( mw, 5, 2 ), 
   " mh ",  str( mh, 5, 2 ), 
   " mr0 ", str( mr0, 5, 2 ), 
   " mr1 ", str( mr1, 5, 2 ), 
   " mr",   str( mr, 5, 2 ), 
   " an0 ", str( an0, 5, 2 ), 
   " an1 ", str( an1, 5, 2 ), 
   " an",   str( an, 5, 2 ), 
   " am0 ", str( am0, 5, 2 ), 
   " am1 ", str( am1, 5, 2 ), 
   " am",   str( am, 5, 2 ), 
   " wrm0",  str( wrm0, 5, 2 ), 
   " wrm1",  str( wrm1, 5, 2 ), 
   " wrmn",  str( wrmn, 5, 2 ), 
   "\n" )



#declare mi = 0;

#while (mi < mn)
  #debug concat( "complete mirror " , str( mi, 3, 1 ), " at a=", str( am_degs, 5, 2 ), "-", str( am_degs + an_degs * (mi + 1), 5, 2 ), " z=", str( mr, 5, 3 ), "\n" )
  object { 
    Mirror(maxMirrorWidth, maxMirrorHeight)  
    translate <0, 0, mr>
    rotate (an_degs * (mi + 1)) * y
  }
  #declare mi = mi + 1;
#end


#if ( (mw > 0) | (mh > 0) )
  #debug concat( "partial mirror dims are ", str( mw, 5, 2 ), "x", str( mh, 5, 2 ), "\n" )
  object { 
    Mirror(mw * maxMirrorWidth, mh * maxMirrorHeight)  
    translate <0, 0, mr>
  }
#end



